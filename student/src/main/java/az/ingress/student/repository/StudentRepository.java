package az.ingress.student.repository;

import az.ingress.student.model.Student;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class StudentRepository {
    private final List<Student>  list;

    public StudentRepository() {
        list = new ArrayList<>(Arrays.asList(
                new Student(1L, "Orkhan", "Muradov", LocalDate.parse("1990-03-25"),"Sumgayit"),
                new Student(2L, "Huseyn", "Guliyev", LocalDate.parse("1989-09-29"),"Baku"),
                new Student(3L, "Teymur", "Gurbanov", LocalDate.parse("2011-02-25"),"Moscow"),
                new Student(4L, "Rasul", "Guliyev", LocalDate.parse("1991-11-06"),"Baku"),
                new Student(5L, "Rauf", "Hasanzade", LocalDate.parse("1992-09-29"),"Nakhchivan")
        ));
    }


    public List<Student> getAllStudents() {
        return list;
    }

    public Student findById(Long id){
        for (Student student : list) {
            if (student.getId().equals(id)) {
                return student;
            }
        }
        return null;
    }

    public Student save(Student s) {
        Student student = Student.builder()
                .id(s.getId())
                .name(s.getName())
                .surname(s.getSurname())
                .dateOfBirth(s.getDateOfBirth())
                .placeOfBirth(s.getPlaceOfBirth())
                .build();
        list.add(student);
        return student;
    }

    public boolean delete(Long id) {
        return list.removeIf(x -> x.getId().equals(id));
    }

    public Student update(Student s) {
        int idx = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().equals(s.getId())) {
                idx = i;
                break;
            }
        }

        Student student = Student.builder()
                .id(s.getId())
                .name(s.getName())
                .surname(s.getSurname())
                .dateOfBirth(s.getDateOfBirth())
                .placeOfBirth(s.getPlaceOfBirth())
                .build();
        list.set(idx, student);
        return student;
    }
}
