package az.ingress.student.controller;

import az.ingress.student.model.Student;
import az.ingress.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {


    private final StudentService service;

    @Autowired
    public StudentController(StudentService service) {
        this.service = service;
    }

    @GetMapping
    public List<Student> findAllStudents() {
        return service.getStudents();
    }

    @PostMapping
    public Student addStudent(@RequestBody Student student) {
        return service.saveStudent(student);
    }

    @GetMapping("{id}")
    public Student findStudentId(@PathVariable Long id) {
        return service.getStudentById(id);
    }

    @PutMapping
    public Student updateProduct(@RequestBody Student product) {
        return service.updateStudent(product);
    }

    @DeleteMapping("{id}")
    public String deleteProduct(@PathVariable Long id) {
        return service.deleteStudent(id);
    }
}
