package az.ingress.student.service;

import az.ingress.student.model.Student;
import az.ingress.student.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;

    @Autowired
    public StudentServiceImpl(StudentRepository repository) {
        this.repository = repository;
    }

    public List<Student> getStudents() {
        return repository.getAllStudents();
    }

    public Student saveStudent(Student student) {
        return repository.save(student);
    }

    public Student getStudentById(Long id) {
        return repository.findById(id);
    }

    public String deleteStudent(Long id) {
       if (repository.delete(id)) {
           return "student removed !! " + id;
       }
       else
       {
           return "student is not removed !! ";
       }
    }

    public Student updateStudent(Student student) {
        return repository.update(student);
    }
}
