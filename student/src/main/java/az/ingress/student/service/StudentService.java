package az.ingress.student.service;

import az.ingress.student.model.Student;

import java.util.List;


public interface StudentService {


     List<Student> getStudents();

     Student saveStudent(Student student);

     Student getStudentById(Long id);

     String deleteStudent(Long id) ;

     Student updateStudent(Student student);

}
